
## *IL1R1+ cancer-associated fibroblasts drive tumor development and immunosuppression in colorectal cancer*

The [`targets`](https://docs.ropensci.org/targets/) pipeline in this
repository provides the source code used in the manuscript.

- `build_data.qmd`: Gathers the data and stores the relevant data for
  each figure as an excel file by Figure and an excel sheet by panel.
- `data_<dataset>_tar.qmd`: Creating an R object to rely on the
  formatted datasets to be used with my helper functions.
- `figure_<x>_tar.qmd`: The target pipelines starting from the Excel
  files stored in the `data` folder and rendering the plots presented in
  the manuscript.
- `figure_<x>_gsea_tar.qmd`: Code for the GSEA presented in the
  manuscript and isolated as independent scripts to executed before the
  figure generating pipeline.

### Helper functions

This pipeline uses some helper functions which are available on Gihub:

- [`ek.plot`](https://github.com/koncina/ek.plot): Helper functions to
  set the panel sizes and save the plots
- [`ek.data`](https://github.com/koncina/ek.data): Helper functions to
  conveniently extract expression data out of `SummarizedExperiment`,
  `ExpressionSet` of `SingleCellExperiment` (`HDF5Array` files)

Other helper functions are defined within this repository in the
`functions_figure_<x>_tar.qmd` files.
