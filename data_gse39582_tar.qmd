## Marisa dataset

```{targets data-marisa}
list(
  tar_file(
    marisa,
    file.path("data",
              "gse39582",
              "eset_GSE39582_1.qs")
  ),
  
  tar_file_read(
    marisa_cms,
    file.path("data",
              "syn4978511",
              "cms_labels_public_all.txt"), 
    read_tsv(!!.x, show_col_types = FALSE) |> 
      filter(dataset == "gse39582") |> 
      select(.sample = sample, 
             cms = CMS_final_network_plus_RFclassifier_in_nonconsensus_samples)
  )
)
```


```{targets marisa-cycaf-scores}
list(
  tar_file_read(
    marisa_icaf,
    "genesets.yaml",
    {
      icaf_gs <- yaml::read_yaml(!!.x) |> 
        enframe(
          name = "gene_set",
          value = "gene_symbol") |> 
        filter(gene_set %in% c("icaf", "il1r1_icaf")) |> 
        unnest("gene_symbol")
      qread(marisa) |> 
        filter_features(gene_symbol %in% icaf_gs$gene_symbol) |> 
        filter_probeset(gene_symbol) |> 
        tidy(sample_vars = "tissue_type",
             feature_vars = "gene_symbol") |> 
        filter(tissue_type == "primary tumour") |> 
        inner_join(icaf_gs,
                   by = "gene_symbol") |> 
        group_by(gene_set, gene_symbol) |> 
        mutate(across(expression, compose(as.numeric, scale))) |> 
        group_by(gene_set, .sample) |> 
        summarise(score = mean(expression), .groups = "drop") |> 
        pivot_wider(names_from = gene_set, values_from = score, names_glue = "{gene_set}_score")
    }
  )
)
```
