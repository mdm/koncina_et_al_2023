---
title: "Figure 1E"
subtitle: "GSEA: IL-1B stimulated CAFs"
format:
  html:
    embed-resources: true
    toc: true
bibliography: references.bib
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(collapse = TRUE, comment = "#>")
library(targets)
Sys.setenv(TAR_PROJECT = "figure_1e")
```

```{r, include = FALSE}
tar_unscript()
```

```{targets globals, tar_globals = TRUE}
options(tidyverse.quiet = TRUE)
library(tarchetypes)
tar_option_set(packages = c("tidyverse", "rappdirs",
                            "qs", "fs",
                            "ek.plot", "ek.data",
                            "ggpubr", "rstatix"))

conflicted::conflicts_prefer(
  dplyr::filter,
  dplyr::rename,
  dplyr::count
)
```

## Introduction

I previously started this GSEA in the project `2022-08-09-caf-il1b-gsea`. In the context of the Nature communications manuscript I'm including the analysis in this project but keep it as an independent targets pipeline which should be executed before the data folder building pipeline.

## Dependencies

::: {.callout-note collapse="true"}
### p200503 dataset

{{< include data_p200503_tar.qmd >}}
:::

## Gene set for the GSEA

```{targets gene-sets}
list(
  tar_target(
    gs_hallmark,
    msigdbr::msigdbr(
      species = "Homo sapiens",
      category = "H"
    ) |> 
      select(gs_name, .feature = ensembl_gene, gene_symbol) 
  ),
  
  
  tar_target(
    gs_highlight, # Highlight gene sets of interest
    tribble(
      ~gs_name,                                        ~gs_lbl,
      "HALLMARK_INFLAMMATORY_RESPONSE",                "Inflammatory response",
      "HALLMARK_INTERFERON_GAMMA_RESPONSE",            "IFNg response",
      "HALLMARK_TNFA_SIGNALING_VIA_NFKB",              "TNFa signaling via NFkB",
      "HALLMARK_IL6_JAK_STAT3_SIGNALING",              "IL6 JAK STAT3 signaling"
    )
  )
)
```

## DGE analysis

Adapting the code of the original analysis in order to use the recently generated p200503 `SummarizedExperiment` object.

```{targets dge}
tar_map(
  
  values = list(
    lbl = list("nf_il1b", "caf_il1b"),
    cid = list(c("NF_ctrl", "NF_IL1B"),
               c("CAF_ctrl", "CAF_IL1B")),
    contrast = list("condition_id_NF_IL1B_vs_NF_ctrl", 
                    "condition_id_CAF_IL1B_vs_CAF_ctrl")
  ),
  
  names = "lbl",
  
  tar_target(
    dge,
    {
      x <- qread(p200503_il1b) |> 
        # Filtering out experiments providing paired NF and CAFs
        filter_samples(experiment %in% c("Exp. 1", "Exp. 3"),
                       condition_id %in% cid)
      
      meta <- sample_data(x) |> 
        select(.sample, experiment, condition_id) |> 
        mutate(across(experiment, as.factor))
      
      SummarizedExperiment::assay(x, i = "raw") |> 
        (\(x) x[rowSums(x) >= 10,])() |> 
        DESeq2::DESeqDataSetFromMatrix(colData = meta,
                                       design = ~ experiment + condition_id) |> 
        DESeq2::DESeq()
    }
  ),
  
  tar_target(
    ashr,
    DESeq2::lfcShrink(dge,
                      contrast,
                      type = "ashr") |>
      as_tibble(rownames = ".feature") |>
      mutate(
        gene_symbol = AnnotationDbi::mapIds(org.Hs.eg.db::org.Hs.eg.db,
                                            keys = .feature,
                                            column = "SYMBOL",
                                            keytype = "ENSEMBL",
                                            multiVals = "first"),
        entrez_id = AnnotationDbi::mapIds(org.Hs.eg.db::org.Hs.eg.db,
                                          keys = .feature,
                                          column = "ENTREZID",
                                          keytype = "ENSEMBL",
                                          multiVals = "first"),
        .after = 1)
  ),
  
  tar_target(
    ashr_ranked,
    arrange(ashr,
            desc(log2FoldChange)) |> 
      rowid_to_column(var = "rank") |> 
      select(rank, .feature, 
             log2fold_change = log2FoldChange)
  ),
  
  tar_target(
    gsea,
    select(ashr_ranked, .feature, log2fold_change) |>
      deframe() |>
      clusterProfiler::GSEA(TERM2GENE = select(gs_hallmark, gs_name, .feature),
                            minGSSize = 5,
                            pvalueCutoff = 1)
  )
)
```

## Pipeline

```{r}
tar_visnetwork()
```

::: {.callout-note collapse="true"}
## Reproducibility

### Date and time

```{r}
Sys.time()
```

### Repository

```{r}
git2r::repository()
```

### Session info

```{r}
sessionInfo()
```
:::

