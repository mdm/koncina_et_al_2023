## Nishida, Calon and Rupp datasets

```{targets data-nicaru}
list(
  tar_files_input(
    nicaru,
    file.path("data",
              "nicaru",
              c("eset_GSE35602_1.qs",
                "eset_GSE39397_1.qs",
                "eset_rupp.qs"))
  )#,
  
  # tar_target(
  #   nicaru_eset_basenames,
  #   recode(
  #     basename(nicaru_eset),
  #     "eset_GSE35602_1.qs" = "Nishida",
  #     "eset_GSE39397_1.qs" = "Calon",
  #     "eset_rupp.qs" = "Rupp"
  #   )
  # )
)
```
