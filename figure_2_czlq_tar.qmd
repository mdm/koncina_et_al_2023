---
title: "Target pipeline to generate the CZLQ figures used in the Nature communications manuscript (revision)"
format: html
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(collapse = TRUE, comment = "#>")
library(targets)
Sys.setenv(TAR_PROJECT = "figure_s2cde")
```

```{r, include = FALSE}
tar_unscript()
```

## Globals

```{targets globals, tar_globals = TRUE}
options(tidyverse.quiet = TRUE)
library(tarchetypes)
tar_option_set(packages = c("tidyverse", "rappdirs", "fs",
                            "HDF5Array", "ComplexHeatmap",
                            "ggpp", "ggtext",
                            "grid", "gtable",
                            "ek.data", "ek.misc", "ek.plot"))

out_path <- file.path("out", "figure_2")

conflicted::conflicts_prefer(
  dplyr::filter,
  dplyr::rename,
  dplyr::count,
  janitor::clean_names
)
```


## Dependencies

::: {.callout-note collapse="true"}
### scRNA-Seq datasets

{{< include data_scrs_czlq_tar.qmd >}}
{{< include data_scrs_tar.qmd >}}


:::



::: {.callout-note collapse="true"}
### General plotting functions

{{< include functions_figure_tar.qmd >}}

:::

::: {.callout-note collapse="true"}
### scRNA-Seq specific plotting functions

{{< include functions_figure_scrnaseq.qmd >}}

:::

**Reviewer 1-2**: Integrated CLZQ dataset



```{targets fig-s2c-functions, tar_globals = TRUE}
cluster_id_colours <- c("#FFD320FF", paletteer::paletteer_d("ggthemes::Classic_10_Medium")) |> # paletteer::paletteer_d("ggthemes::stata_economist", n = 11) |>  #ggthemes::hc_darkunica
  as.character() |> 
  purrr::set_names(0:10)
```


```{targets fig-s2c}
list(
  tar_target(
    fig_s2c_type,
    ggplot(czlq_metadata_all, 
           aes(x = umap_1, y = umap_2, colour = cell_type_mdm)) +
      geom_point(size = 0.2,  stroke = 0) +
      scale_colour_manual(
        limits = c("Epithelial cells", "Fibroblasts",
                   "Endothelial cells", "Enteric glial cells",
                   "T cells", "B cells",
                   "Myeloid cells", "Mast cells"),
        values = my_colour(palette = "main")) +
      theme_bw(10) +
      theme(
        aspect.ratio = 1,
        axis.title = element_blank(),
        legend.text = element_text(size = 8), 
        #legend.margin = margin(0, 0, 0, -18),
        legend.box.margin = margin(-15, 0, 0, -35),
        axis.ticks = element_blank(),
        axis.text = element_blank(),
        panel.grid = element_blank(), 
        legend.spacing.x = unit(1, "mm"),
        legend.position = "bottom",
        plot.margin = margin(5, 1, 5, 10),
        legend.key.width = unit(1, "mm"),
        legend.key.height = unit(0.5, "lines")
      ) +
      guides(colour = guide_legend(ncol = 2,
                                   label.hjust = 0,
                                   override.aes = list(size = 3))) +
      labs(x = "UMAP 1", 
           y = "UMAP 2",
           colour = NULL)
  ),
  
  
  tar_file(
    fig_s2c_type_png,
    add_axis_nofacet(fig_s2c_type) |> 
      set_panel_size(p = NULL,
                     width = unit(3.5, "cm"),
                     height = unit(3.5, "cm")) |>
      write_plot(file.path(out_path, "figure_s2c_type.png"))
    
  ),
  
   tar_target(
     fig_s2c_tissue,
     group_by(czlq_metadata_all, tissue_type, cell_type_mdm) |> 
       mutate(rank = seq_along(.sample)) |> 
       ungroup() |> 
       arrange(desc(rank)) |> 
       ggplot(aes(x = umap_1,
                  y = umap_2,
                  colour = tissue_type)) +
       geom_point(size = 0.2, stroke = 0) +
       scale_colour_manual(values = c(normal = "#33BB44", tumor = "#DD6666")) +
       theme_bw(10) +
       theme(
        aspect.ratio = 1,
        axis.title = element_blank(),
        legend.text = element_text(size = 8), 
        #legend.margin = margin(0, 0, 0, -18),
        legend.box.margin = margin(-15, 0, 0, 0),
        axis.ticks = element_blank(),
        axis.text = element_blank(),
        panel.grid = element_blank(), 
        legend.spacing.x = unit(1, "mm"),
        legend.position = "bottom",
        plot.margin = margin(5, 1, 5, 1),
        legend.key.width = unit(1, "mm"),
        legend.key.height = unit(0.5, "lines")
      ) +
       guides(colour = guide_legend(ncol = 2,
                                    label.hjust = 0,
                                    override.aes = list(size = 3))) +
       labs(x = "UMAP 1", 
            y = NULL,
            colour = NULL)
   ),
  
  tar_file(
    fig_s2c_tissue_png,
    add_axis_nofacet(fig_s2c_tissue, draw_axis = FALSE) |> 
      set_panel_size(p = NULL,
                   width = unit(3.5, "cm"),
                   height = unit(3.5, "cm")) |>
      write_plot(file.path(out_path, "figure_s2c_tissue.png"))
    
  ),
  
  tar_target(
    fig_s2c_cohort,
    mutate(czlq_metadata_all, cohort = case_when(
      dataset == "Lee" & str_detect(.sample, "^SMC") ~ "SMC (Lee)",
      dataset == "Lee" & str_detect(.sample, "^KUL") ~ "KUL3 (Lee)",
      dataset == "Qian" ~ "Qian",
      dataset == "CLZ" & str_detect(.sample, "COLE") ~ "Cole",
      dataset == "CLZ" ~ "Zhang"
    )) |>
      group_by(cohort, cell_type_mdm) |> 
      mutate(rank = seq_along(.sample)) |> 
      ungroup() |> 
      arrange(desc(rank)) |> 
      ggplot(aes(x = umap_1,
                 y = umap_2,
                 colour = cohort)) +
      geom_point(size = 0.2, stroke = 0) +
      paletteer::scale_colour_paletteer_d("RColorBrewer::Accent", limits = c("Cole", "Zhang", "SMC (Lee)", "KUL3 (Lee)", "Qian")) +
      theme_bw(10) +
       theme(
        aspect.ratio = 1,
        axis.title = element_blank(),
        legend.text = element_text(size = 8), 
        legend.box.margin = margin(-15, 0, 0, 0),
        axis.ticks = element_blank(),
        axis.text = element_blank(),
        panel.grid = element_blank(), 
        legend.spacing.x = unit(1, "mm"),
        plot.margin = margin(5, 5, 5, 1),
        legend.position = "bottom",
        legend.key.width = unit(1, "mm"),
        legend.key.height = unit(0.5, "lines")
      ) +
      guides(colour = guide_legend(ncol = 2,
                                   label.hjust = 0,
                                   override.aes = list(size = 3))) +
      labs(x = "UMAP 1", 
           y = NULL,
           colour = NULL)
  ),
  
  tar_file(
    fig_s2c_cohort_png,
    add_axis_nofacet(fig_s2c_cohort, draw_axis = FALSE) |> 
      set_panel_size(p = NULL,
                     width = unit(3.5, "cm"),
                     height = unit(3.5, "cm")) |>
      write_plot(file.path(out_path, "figure_s2c_cohort.png"))
    
  )
)
```

## Figure S2D: CAF clusters

```{targets fig-s2d}
list(
  tar_target(
    fig_s2d_cluster,
    ggplot(czlq_metadata_caf,
           aes(x = umap_1, y = umap_2,
               colour = cluster_id)) +
      geom_point(size = 0.5,  stroke = 0) +
      geom_label(data = ~group_by(.x, cluster_id) |>
                   summarise(across(c(umap_1, umap_2), mean)),
                 aes(label = cluster_id, fill = cluster_id),
                 label.padding = unit(0.1, "lines"),
                 label.r = unit(0.2, "lines"),
                 label.size = 0.15,
                 alpha = 0.5,
                 size = 8 / ggplot2:::.pt,
                 colour = "black"
      ) +
      scale_colour_manual(values = !!cluster_id_colours) +
      scale_fill_manual(values = !!cluster_id_colours) +
      #paletteer::scale_colour_paletteer_d("ggthemes::hc_darkunica") +
      #paletteer::scale_fill_paletteer_d("ggthemes::hc_darkunica") +
      theme_bw(10) +
      
      theme(
        aspect.ratio = 1,
        axis.title = element_blank(),
        legend.text = element_text(size = 8), 
        #legend.margin = margin(0, 0, 0, -18),
        legend.box.margin = margin(-15, 0, 0, 0),
        axis.ticks = element_blank(),
        axis.text = element_blank(),
        panel.grid = element_blank(), 
        legend.spacing.x = unit(1, "mm"),
        legend.position = "none",
        plot.margin = margin(5, 1, 5, 5),
        legend.key.width = unit(1, "mm"),
        legend.key.height = unit(0.5, "lines")
      ) +
      labs(x = "UMAP 1", y = "UMAP 2")
  ),
  
  tar_file(
    fig_s2d_cluster_png,
    add_axis_nofacet(fig_s2d_cluster, draw_axis = TRUE) |> 
      set_panel_size(p = NULL,
                     width = unit(3, "cm"),
                     height = unit(3, "cm")) |>
      write_plot(file.path(out_path, "figure_s2d_cluster.png"))
    
  ),
  
  tar_target(
    fig_s2d_group,
    ggplot(czlq_metadata_caf,
           aes(x = umap_1, y = umap_2,
               colour = caf_group)) +
      geom_point(size = 0.5,  stroke = 0) +
      scale_colour_manual(values = my_colour(palette = "caf"),
                          limits = c("myCAF", "iCAF", "IL1R1+ iCAF"),
                          labels = c("myCAF", "iCAF", "IL1R1<sup>+</sup> iCAF")) +
      theme_bw(10) +
      theme(
        aspect.ratio = 1,
        axis.title = element_blank(),
        legend.box.margin = margin(0, 0, 0, 0),
        axis.ticks = element_blank(),
        axis.text = element_blank(),
        panel.grid = element_blank(), 
        legend.text = element_markdown(size = 8),
        legend.spacing.x = unit(1, "mm"),
        legend.position = "bottom",
        plot.margin = margin(5, 1, 5, 1),
        legend.key.width = unit(1, "mm"),
        legend.key.height = unit(0.5, "lines")
      ) +
      labs(x = "UMAP 1", y = NULL, colour = NULL) +
      guides(colour = guide_legend(ncol = 1,
                                   label.hjust = 0,
                                   override.aes = list(size = 3)))
  ),
  
  tar_file(
    fig_s2d_group_png,
   # add_axis_nofacet(fig_s2d_group, draw_axis = FALSE) |> 
      set_panel_size(fig_s2d_group,
                     width = unit(3, "cm"),
                     height = unit(3, "cm")) |>
      write_plot(file.path(out_path, "figure_s2d_group.png"))
    
  ),
  
  tar_target(
    fig_s2d_cohort,
    left_join(czlq_metadata_caf,
              select(czlq_metadata_all, .sample, dataset),
              by = ".sample") |> 
      mutate(cohort = case_when(
        dataset == "Lee" & str_detect(.sample, "^SMC") ~ "SMC",
        dataset == "Lee" & str_detect(.sample, "^KUL") ~ "KUL3",
        dataset == "Qian" ~ "Qian",
        dataset == "CLZ" & str_detect(.sample, "COLE") ~ "Cole",
        dataset == "CLZ" ~ "Zhang"
      )) |>
      group_by(cohort, cluster_id) |> 
      mutate(rank = seq_along(.sample)) |> 
      ungroup() |> 
      arrange(desc(rank)) |> 
      ggplot(
        aes(x = umap_1, y = umap_2,
            colour = cohort)) +
      geom_point(size = 0.5,  stroke = 0) +
      paletteer::scale_colour_paletteer_d("RColorBrewer::Accent", limits = c("Cole", "Zhang", "SMC", "KUL3", "Qian")) +
      theme_bw(10) +
      theme(
        aspect.ratio = 1,
        axis.title = element_blank(),
        legend.text = element_text(size = 8), 
        legend.box.margin = margin(0, 0, 0, 0),
        axis.ticks = element_blank(),
        axis.text = element_blank(),
        panel.grid = element_blank(), 
        legend.spacing.x = unit(1, "mm"),
        legend.position = "bottom",
        plot.margin = margin(5, 5, 5, 1),
        legend.key.width = unit(1, "mm"),
        legend.key.height = unit(0.5, "lines")
      ) +
      labs(x = "UMAP 1", y = NULL, colour = NULL) +
      guides(colour = guide_legend(ncol = 2,
                                   label.hjust = 0,
                                   override.aes = list(size = 3)))
  ),
  
  tar_file(
    fig_s2d_cohort_png,
    #add_axis_nofacet(fig_s2d_cohort, draw_axis = FALSE) |> 
      set_panel_size(fig_s2d_cohort,
                     width = unit(3, "cm"),
                     height = unit(3, "cm")) |>
      write_plot(file.path(out_path, "figure_s2d_cohort.png"))
    
  )
)
```

## Figure S2E

Showing _IL1R1_ expression in the different CAF clusters as violin plots with pie charts. The colour code is the one used in the cluster ID UMAP representation.

```{targets fig-s2e-functions, tar_globals = TRUE}
# plot_single_pie <- function(percent, ccc) {
#   tibble(yes = percent, no = 100 - percent) %>%
#     pivot_longer(names_to = "expression", values_to = "percent", c(yes, no)) %>% 
#     ggplot(aes(x = "", y = percent, fill = expression)) +
#     geom_col(width = 4, color = 1, linewidth = 0.5) +
#     coord_polar(theta = "y", clip = "off") +
#     theme_void() +
#     theme(legend.position = "none") +
#     scale_fill_manual(values = c(yes = ccc, no = "gray")) 
# }


plot_violin_pie_1d <- function(data, x, y, colour, vp_value, my_palette = NULL) {
  
  n_observations <- pull(data, {{x}}) |> 
    n_distinct()
  
  message(n_observations)
  
  vp_value <- 0.8 * 1/n_observations
  
  if (is.null(my_palette)) {
    my_palette <-  set_names(
      viridisLite::viridis(n_observations, direction = 1),
      pull(data, {{x}}) |>
        sort() |> 
        levels()
    )
  }  

  
  max_height <- pull(data, {{y}}) |> 
    max()
  
  pie_tbl <- group_by(data, {{x}}) |> 
    summarise(percent_expressing = 100 * sum(logcounts > 0) / n(),
              .groups = "drop") %>% 
    mutate(.colour = my_palette[as.character({{x}})],
           pie = map2(percent_expressing, .colour, 
                      compose(ggplotGrob, plot_single_pie)))
  
  ggplot(data, aes(x = {{x}}, y = {{y}})) +
    geom_violin(aes(fill = {{colour}}), scale = "width", draw_quantiles = 0.5) +
    geom_grob(data = pie_tbl, aes(x = {{x}}, y  = 1.08 * max_height, label = pie), 
              vp.width = 0.15, vp.height = 1) +
    geom_text(data = pie_tbl, aes(x = {{x}}, y = 1.23 * max_height,
                                  label = paste0(round(percent_expressing, 1), "%"), hjust = 0),
              size = 6 / ggplot2:::.pt) +
    scale_fill_manual(values = my_palette) +
    scale_y_continuous(expand = expansion(mult = c(0, 0.3)), breaks = 0:3) +
    labs(y = "Norm. counts", x = "Cluster ID<BR>(ranked by mean _IL1R1_)")
}

```

```{targets fig-s2e}
list(
  tar_target(
    fig_s2e_data,
    semi_join_samples(czlq, czlq_metadata_caf, by = ".sample") |> 
      filter_features(.feature == "IL1R1") |> 
      tidy(assay = "logcounts") |> 
      inner_join(czlq_metadata_caf, by = ".sample") |> 
      mutate(
        across(
          cluster_id, \(x) fct_reorder(x, 
                                       logcounts,
                                       .desc = FALSE,
                                       .fun = mean)
        )
      )
  ),
  
  tar_target(
    fig_s2e,
    plot_violin_pie_1d(fig_s2e_data, cluster_id, logcounts, cluster_id, 2 / 8, my_palette = !!cluster_id_colours) +
      theme_classic(10) +
      theme(legend.position = "none",
            axis.title.x = element_markdown(),
            axis.title.y = element_markdown()) +
      coord_flip()
  ),
  
  tar_file(
    fig_s2e_png,
    set_panel_size(fig_s2e, height = unit(6 * 0.8, "cm"), width = unit(3, "cm")) |> 
      write_plot(file.path(out_path, "figure_s2e.png"))
  )
)
```

## Figure r1-2: for reviewing purposes

To further illustrate that the IL1R1^+^ CAFs we identified previously overlap with the IL1R1^+^ iCAF cluster identified in the integrated dataset we can map the old labels to the new CAF clusters.

```{r}
# library(tidyverse)
# select(tar_read(czlq_metadata_all), .sample, cell_type_mdm_v1) |> 
#       inner_join(tar_read(czlq_metadata_caf), by = ".sample") |> 
#       left_join(select(tar_read(scrs_metadata_tf), .sample, cluster_id_v1 = cluster_id, caf_group_v1 = cluster_group), by = ".sample") |> #v1 labels
#       mutate(caf_lbl_v1 = if_else(is.na(caf_group_v1), 
#                                   cell_type_mdm_v1, caf_group_v1),
#              caf_lbl_v1 = fct_lump_prop(caf_lbl_v1, 0.05, other_level = "other"),
#              caf_lbl_v1 = fct_relevel(caf_lbl_v1, 
#                                       "mCAF", "cyCAF-1", "cyCAF-2",
#                                       "other"))
```


```{targets caf-cluster-barcharts}
list(
  tar_target(
    r1_2a,
    select(czlq_metadata_all, .sample, cell_type_mdm_v1) |> 
      inner_join(czlq_metadata_caf, by = ".sample") |> 
      left_join(select(scrs_metadata_tf, .sample, cluster_id_v1 = cluster_id, caf_group_v1 = cluster_group), by = ".sample") |> #v1 labels
      mutate(caf_lbl_v1 = if_else(is.na(caf_group_v1), 
                                  cell_type_mdm_v1, caf_group_v1),
             caf_lbl_v1 = fct_lump_prop(caf_lbl_v1, 0.05, other_level = "other"),
             caf_lbl_v1 = fct_relevel(caf_lbl_v1, 
                                      "mCAF", "cyCAF-1", "cyCAF-2",
                                      "other")) |> 
      ggplot(aes(x = cluster_id, fill = caf_lbl_v1)) +
      geom_bar() +
      theme_classic(10) +
      theme(strip.text.y.right = element_markdown(angle = 0),
            strip.background = element_blank(),
            legend.margin = margin(),
            plot.margin = margin(5, 1, 5, 5),
            legend.box.margin = margin(),
            legend.position = "bottom",
            legend.text = element_markdown(),
            legend.key.height = unit(0.75, "lines"),
            legend.key.width = unit(0.75, "lines")) +
      labs(x = "cluster ID (integrated)",
           y = "Nb of CAFs",
           fill = "Labels defined\nin non-integrated") +
      scale_fill_manual(values = c(my_colour( palette = "caf"), other = "gray"),
                        limits = c("myCAF", "iCAF", "IL1R1+ iCAF"),
                        labels = c("myCAF", "iCAF", "IL1R1<sup>+</sup> iCAF")) +
      coord_flip() +
      facet_grid(fct_relevel(caf_group, "myCAF", "iCAF") ~ ., scales = "free_y", space = "free_y", 
                 labeller = labeller(.rows = \(x) str_replace(x, "\\+", "<sup>+</sup>"))) +
      guides(fill = guide_legend(ncol = 2, title.position = "top",
                                 label.hjust = 0)) 
  ),
  
  tar_file(
    r1_2a_png,
    set_panel_size(r1_2a, 
                   height = c(2, 8, 1) * unit(3, "mm"), width = unit(3, "cm")) |> 
      write_plot(file.path("out", "revisions", "r1_2a.png"))
  ),
  
  tar_target(
    r1_2b,
    select(czlq_metadata_all, .sample, dataset) |> 
      inner_join(czlq_metadata_caf, by = ".sample") |> 
      ggplot(aes(x = cluster_id, fill = dataset)) +
      geom_bar() +
      theme_classic(10) +
      theme(strip.text.y.right = element_blank(),#element_markdown(angle = 0),
            strip.background = element_blank(),
            legend.position = "bottom",
            plot.margin = margin(5, 5, 5, 1),
            legend.margin = margin(),
            legend.box.margin = margin(),
            legend.spacing.x = unit(1, "mm"),
            legend.key.height = unit(0.75, "lines"),
            legend.key.width = unit(0.75, "lines")) +
      labs(x = NULL, #"cluster ID (integrated)",
           y = "Nb of CAFs",
           fill = "Dataset") +
      paletteer::scale_fill_paletteer_d("RColorBrewer::Accent") +
      coord_flip() +
      facet_grid(fct_relevel(caf_group, "myCAF", "iCAF") ~ ., scales = "free_y", space = "free_y") +
      guides(fill = guide_legend(nrow = 1,
                                   label.hjust = 0, title.position = "top",
                                   override.aes = list(size = 3))) 
  ),
  
  tar_file(
    r1_2b_png,
    set_panel_size(r1_2b, 
                   height = c(2, 8, 1) * unit(3, "mm"), width = unit(3, "cm")) |> 
      write_plot(file.path("out", "revisions", "r1_2b.png"))
  )
)
```

