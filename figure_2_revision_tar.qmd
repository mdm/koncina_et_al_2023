---
title: "Figure 2 (revisions)"
format:
  html:
    embed-resources: true
    toc: true
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(collapse = TRUE, comment = "#>")
library(targets)
Sys.setenv(TAR_PROJECT = "revisions")
```

```{r, include = FALSE}
tar_unscript()
```

```{targets globals, tar_globals = TRUE}
options(tidyverse.quiet = TRUE)
library(tarchetypes)
tar_option_set(packages = c("tidyverse", "rappdirs",
                            "qs", "fs",
                            "readxl", "janitor",
                            "ggtext", "ggh4x",
                           # "magick", "ggrepel",
                           # "gtable", "ggtext",
                           # "ggbeeswarm", "grid",
                            #"ggh4x", "ggpp",
                            "ek.plot", "ek.data",
                            "ek.misc",
                            "ggpubr", "rstatix"))

out_path <- file.path("out", Sys.getenv("TAR_PROJECT"))

conflicted::conflicts_prefer(
  dplyr::filter,
  dplyr::rename,
  dplyr::count
)
```


## Dependencies


::: {.callout-note collapse="true"}
### General plotting functions

{{< include functions_figure_tar.qmd >}}

:::

## Figure 2H

Trying to address the reviewer's comment

> The authors show that CMS4 tumors have a cyCAF-2 signature. Does the inhouse TMA contain mostly CMS4 tumors?

We now sequenced the RNA of our SOCS cohort samples and were able to use `CMSCaller` to identify the CMS subtypes.

## CMS subtypes in the SOCS cohort


```{targets fig-2h-revision}
list(
  
  tar_file_read(
    fig_2h_socs_clinical,
    file.path(user_data_dir("data"),
              "mdm",
              "socs",
              "socs_clinical_210906.xlsx"),
    read_excel(!!.x, sheet = "kit") |> 
      filter(sample == "tissue") |> 
      select(subject, kit_id) |> 
      inner_join(read_excel(!!.x, sheet = "socs_clinical_dev"),
                 by = "subject")
  ),
  
  
  tar_file_read(
    fig_2h_socs_cms,
    file.path(
      user_data_dir("data"),
      "datasets",
      "2022-11-29-socs-rnaseq-cms",
      "socs_cms.csv"),
    read_csv(!!.x, show_col_types = FALSE,
             col_types = "ccc")
  ),
  
  tar_file_read(
    fig_2h_data,
    file.path(
      rappdirs::user_data_dir("data"),
      "socs", "2020-08-11-socs-tma",
      c("IL-1_beta_TMA_4_new,_8,_10,_11,_12.xls",
        "FAP_TMA_4_new,_8,10,11,12.xls",
        "SMA_TMA_4_new,_8,_10,_11,_12.xls")
    ),
    map_dfr(!!.x, \(x) read_xls(x, .name_repair = janitor::make_clean_names)) |> 
      select(staining = folder, 
             title,
             positive_segmented_tissue_area_mm2,
             total_tissue_area_mm2 ) |> 
      separate(title, c(NA, "socs_id"), sep = "\\|") |>
      mutate(kit_id = str_extract(socs_id, "\\d+(?=-SOCS)"),
             kit_id = str_remove(kit_id, "^0"),
             tissue_type = str_extract(socs_id, "[TD](?=\\d{2})"),
             tissue_type = recode(tissue_type,
                                  `T` = "tumor", `D` = "normal"),
             across(staining, tolower),
             staining = recode(staining,
                               "il1 beta" = "il1b",
                               "sma alpha" = "acta2")) |>
      filter(tissue_type == "tumor") |>
      group_by(staining, kit_id) |> 
      summarise(across(ends_with("area_mm2"), sum), .groups = "drop") |> 
      mutate(percent_positive = 100 * positive_segmented_tissue_area_mm2 / total_tissue_area_mm2) |> 
      pivot_wider(id_cols = "kit_id",
                  names_from = staining,
                  values_from = percent_positive) |>
      inner_join(select(fig_2h_socs_clinical, subject, kit_id, t, n, m,
                        tumour_localisation, gender, age) , by = "kit_id") |>
      left_join(fig_2h_socs_cms, c("kit_id", "subject")) |>
      # Make sure a single Kit ID is provided for each patient...
      assertr::verify(n_distinct(kit_id) == n_distinct(subject)) |>
      mutate(stage = tnm_to_stage(t, n, m,
                                  substage = FALSE,
                                  mx_as_m0 = TRUE)) |> 
      replace_na(list(cms = "Not subtyped")) 
  ),
  
  tar_map(
    values = list(x_axis = c(quote(fap), quote(acta2)),
                  x_axis_lbl = c("FAP", "αSMA"),
                  y_axis = c(quote(il1b), quote(il1b)),
                  y_axis_lbl = c("IL-1β", "IL-1β")

    ),
    names = "x_axis",

    tar_target(
      fig_2h_cms,
      filter(fig_2h_data, str_detect(cms, "^CMS")) |> 
        ggplot(aes(x = x_axis, y = y_axis, colour = cms)) +
        geom_point(size = 0.5) +
        ggh4x::facet_grid2(. ~ cms, axes = "all", remove_labels = "y") +
        geom_smooth(method = "lm", formula = "y ~ x", se = FALSE, linewidth = 1) +
        ggpubr::stat_cor(aes(label = ..r.label..),
                         cor.coef.name = "r",
                         size = 8 / ggplot2:::.pt,
                         hjust = 1,
                         label.x = 95,
                         label.y = 25) +
        ggpubr::stat_cor(aes(
          label = scales::pvalue(..p..,
                                 accuracy = 0.001,
                                 add_p = TRUE)),
          size = 8 / ggplot2:::.pt,
          output.type = "text",
          hjust = 1,
          #label.x.npc = 0.05,
          #label.y.npc = 0.8
          label.x = 95,
          label.y = 10
        ) +
        theme_classic(10) +
        theme(
          strip.background = element_blank(),
          legend.position = "none",
          panel.spacing.x = unit(3, "mm"),
          plot.margin = margin(5, 10, 5, 5),
          axis.title.x = element_markdown(),
          axis.title.y = element_markdown()
        ) +
        scale_colour_manual(values = ek.misc::colours_cms()) +
        scale_y_continuous(limits = c(0, 100), expand = expansion(mult = c(0, 0.1))) +
        scale_x_continuous(limits = c(0, 100), expand = expansion()) +
        labs(x = paste0(x_axis_lbl, "<sup>+</sup> area (%)"),
             y =  paste0(y_axis_lbl, "<sup>+</sup> area (%)"))

    ),

    tar_file(
      fig_2h_cms_png,
      set_panel_size(fig_2h_cms,
                     width = unit(2.5, "cm"),
                     height = unit(2.5, "cm")) |>
        write_plot(file.path(out_path,
                             paste0("figure_2h_cms_",
                                    substitute(x_axis), "_",
                                    substitute(y_axis),  ".png")
        )
        )
    )
  )
)
```

## Supplemental table 2 updated with the CMS informations

Updating the supplemental table 2 which refers to the figure 2H (TMA). Here we provide the summary statistics of our SOCS cohort.

Using the function developed within the SOCS pipeline to generate the summary stats.

```{targets table-s2-function, tar_globals = TRUE}
get_table_s2 <- function(socs_clinical) {
  
  cms_stat <-   filter(socs_clinical, str_detect(cms, "^CMS|NOLBL")) |> 
    count(cms) |> 
    mutate(n_cms = glue::glue("{n} ({scales::percent_format(suffix = '\\\\%')(n / sum(n))})")) |> 
    select(-n)
  
  mutate(socs_clinical,
         age = if_else(age > 65, "$>$ 65", "$\\leq$ 65"), #
         age = forcats::fct_relevel(age, "$\\leq$ 65"),
         #stage = tnm_to_stage(t, n, m, substage = FALSE, mx_as_m0 = TRUE),
         tumour_localisation = recode(tumour_localisation, "right colon" = "proximal colon"),
         tumour_localisation = fct_collapse(tumour_localisation,
                                            `proximal colon` = c("cecum",
                                                                 "ascending colon",
                                                                 "hepatic flexure",
                                                                 "transverse colon"),
                                            `distal colon` = c("splenic flexure",
                                                               "descending colon",
                                                               "sigmoid colon"))) |> 
    arrange(tumour_localisation) |> 
    select(subject, age, gender, stage, tumour_localisation, cms) |> 
    pivot_longer(cols = c(age, gender, stage, tumour_localisation, cms)) |> 
    count(name, value) |> 
    mutate(value = fct_relevel(value,
                               "$\\leq$ 65", "$>$ 65",
                               as.character(0:4),
                               "proximal colon", "distal colon", "rectosigmoid", "rectum"),
           name = fct_relevel(name, "age", "gender", "stage", "tumour_localisation", "cms"),
           name = fct_recode(name, "Age" = "age", "Gender" = "gender",
                             "Stage" = "stage", "Tumor localisation" = "tumour_localisation",
                             "CMS" = "cms")) |> 
           #name = compose(str_to_sentence, str_replace_all)(name, "_", " "),
           #name = fct_relevel(name, "Age", "Gender", "Stage", "Tumour localisation")) |> 
    arrange(name, value) |> 
    # Not very elegant but I need to move on
    left_join(cms_stat, by = c("value" = "cms")) |> 
    mutate(n =  if_else(is.na(value) | !str_detect(value, "^CMS|NOLBL"),as.character(n), n_cms)) |> 
    select(-n_cms)
}

write_table_s2_tex <- function(socs_summary_stats, summary_tex_header, out_tex) {
  
  tbl_tex <- mutate(socs_summary_stats,
                    across(value, fct_explicit_na, "unknown")) |> 
    group_by(name) |> 
    nest() |> 
    mutate(tbl_section = map(data, glue::glue_data, "\\hspace{{1em}}{value} & {n}\\\\"),
           tbl_section_header = map(name, ~ c("\\addlinespace[0.3em]", glue::glue("\\multicolumn{{2}}{{l}}{{\\textbf{{{.x}}}}}\\\\")))) |> 
    summarise(tbl_tex = c(tbl_section_header, tbl_section), .groups = "drop") |> 
    pull(tbl_tex) %>% 
    flatten_chr() 

  c(read_lines(summary_tex_header),
    tbl_tex,
    c("\\bottomrule", "\\end{tabular}", "\\end{document}")) |> 
    write_lines(out_tex)
  
  out_tex
}

```

```{targets table-s2}
list(
  tar_target(
    table_s2_data,
    get_table_s2(fig_2h_data)
  ),
  
  tar_file(
    table_s2_tex_header,
    file.path("data", "socs", "summary_table_header.tex")
  ),

  tar_file(
    table_s2_tex,
    write_table_s2_tex(table_s2_data, table_s2_tex_header, 
                       out_tex = file.path(out_path, "table_s2.tex"))
  ),

  tar_file(
    table_s2_pdf,
    tinytex::latexmk(table_s2_tex, engine = "xelatex")
  )
)
```

## Colocalization analysis

Addition to figure 2I. We might propose a reduced quantification in the main figure and add additional M coefficients as well as pictures in the supplements.

```{targets fig-2i-new}
list(
  tar_file_read(
    fig_2i_new_socs,
    file.path(
      rappdirs::user_data_dir("data"),
      "caf",
      "2023-02-28-caf-il1r1-natcom",
      "eliane",
      "eliane_colocalization_finder.txt"
    ),
    read_delim(!!.x, show_col_types = FALSE, delim = ";",
               col_names = c("path", "c1", "c2", "name_1", "name_2",
                             "pearson_r", "average_1", "average_2",
                             "sigma_1", "sigma_2", "overlap_r", "k1",	"k2",
                             "m1",	"m2",	"m1_norm",	"m2_norm",
                             "slope",	"intercept",	"nb_pixels",
                             "percent_pixels",
                             "min_i1",	"max_i1",
                             "min_i2",	"max_i2",
                             "pic1",	"pic2")) |> 
      mutate(patient = str_extract(path, "(?<=LSM710 raw images/)\\d+"), 
             picture = str_extract(path, "(?<=x_).*$"),
             .before = 1) |> 
      filter(!is.na(patient)) |> 
      select(-starts_with("path")) |> 
      mutate(across(starts_with("name"),
                    \(x) recode(x, "ch0" = "il1r1", "ch1" = "pdpn",
                                "ch2" = "dapi"))) |> 
      select(name_1, name_2,  patient, m1_norm, m2_norm) 
  ),
  
  # Colocalization analysis from the Vienna pictures shown in the manuscript
  tar_file_read(
    fig_2i_new_data,
    file.path(
      "data",
      "confocal",
      "colocalization_finder.txt"
    ),
    read_delim(!!.x, show_col_types = FALSE, delim = ";",
               col_names = c("path_1", "path_2", "name_1", "name_2",
                             "pearson_r", "average_1", "average_2",
                             "sigma_1", "sigma_2", "overlap_r", "k1",	"k2",
                             "m1",	"m2",	"m1_norm",	"m2_norm",
                             "slope",	"intercept",	"nb_pixels",
                             "percent_pixels",
                             "min_i1",	"max_i1",
                             "min_i2",	"max_i2",
                             "pic1",	"pic2")) |> 
      mutate(id = str_extract(path_1, "\\d/Tumor\\d+"),
             mag = str_extract(path_1, "2x")) |> 
      separate_wider_delim(id, delim = "/Tumor",
                           names =  c("picture", "patient")) |> 
      filter(!is.na(patient)) |> 
      replace_na(list(mag = "1x")) |> 
      select(-starts_with("path")) |> 
      mutate(across(starts_with("name"),
                    \(x) recode(x, "ch00" = "dapi", "ch01" = "pdpn",
                                "ch02" = "acta2", "ch03" = "il1r1"))) |> 
      filter(mag == "2x") |> 
      select(name_1, name_2, patient, m1_norm, m2_norm) |> 
      # bind_rows(socs_data, .id = "cohort") |> 
      # mutate(cohort = recode(cohort, `1` = "Vienna", `2` = "SOCS")) |> 
      pivot_longer(names_to = "measure", values_to = "manders", ends_with("norm"), 
                   names_transform = \(s) str_remove(s, "_norm") |> toupper()) |> 
      mutate(
        across(starts_with("name"), toupper),
        top = if_else(measure == "M1", name_2, name_1),
        bottom = if_else(measure == "M2", name_2, name_1),
        m_lbl = glue::glue("$\\frac{{{top} \\cap {bottom} }}{{{bottom}}} $")) |> 
      filter(bottom == "IL1R1", top == "PDPN") 
  ),
  
  tar_target(
    fig_2i_new,
      ggplot(fig_2i_new_data, aes(x = "", y = manders)) +
      geom_violin(scale = "width", trim = FALSE, draw_quantiles = 0.5) +
      ggbeeswarm::geom_quasirandom(size = 1, aes(colour = patient, group = measure)) +
      # facet_wrap(~ m_lbl,
      #            labeller = as_labeller(\(x) latex2exp::TeX(x),
      #                                   default = label_parsed)) +
      # facet_wrap2(
      #   #~ m_lbl + cohort,
      #   ~ m_lbl,
      #   scales = "free_x",
      #   labeller = as_labeller(\(x) latex2exp::TeX(x),
      #                          default = label_parsed),
      #   axes = "all", remove_labels = "all") +
      #facet_nested(~ name + measure, scales = "free_x") +
      scale_y_continuous(limits = c(0, 1), expand = expansion(mult = c(0, 0.05))) +
      paletteer::scale_colour_paletteer_d("ggthemes::Superfishel_Stone") +
      theme_classic(10) +
      theme(
        axis.text.x = element_blank(),
        axis.ticks.x = element_blank(),
        strip.text = element_text(size = 6),
        plot.title.position = "plot",
        plot.title = element_text(hjust = 1, size = 8),
        strip.background = element_blank(),
        legend.key.height = unit(0.5, "lines"),
        legend.position = "none"
      ) +
      labs(x = NULL, y = "Mander's coef.", # ficient
           title = latex2exp::TeX(unique(fig_2i_new_data$m_lbl)))
  ),
  
  tar_file(
    p6_plot_png,
    set_panel_size(fig_2i_new, width = unit(1.5, "cm"), height = unit(2.5, "cm")) |>
      write_plot(file.path(out_path, "figure_2i_new.png"))
  )
  
)
```
    


## Pipeline

```{r}
tar_visnetwork()
```

::: {.callout-note collapse="true"}
## Reproducibility

### Date and time

```{r}
Sys.time()
```

### Repository

```{r}
git2r::repository()
```

### Session info

```{r}
sessionInfo()
```
:::

