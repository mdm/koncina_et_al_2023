## scRNA-Seq processing

This folder contains the files with the scRNA-Seq data processing performed on the HPC.

### CLZ

COLE1: (COLE1_preprocess_harmony_stageused.Rmd)

(Description of preprocess upstream to be asked to those who provided the count matrices)

Count matrices were further processed and analyzed using R and the package Seurat.  
Ribosomal genes (starting with "RPS" or "RPL") were removed at import. After quality control, only cells with nFeature_RNA > 200, nFeature_RNA < 2000 and percent.mt < 15 were kept for analysis. Data were then lognormalized and scaled, regressing out covariates "nCount_RNA", and "percent.mt". PCA reduction was performed using the 2000 most variable features as defined by default parameters of FindVariableFeatures Seurat function. PCA projection was adjusted for variation between patients using harmony, and UMAP reduction was computed on those corrected components, using the 40 first dimensions. Reduced data was further segmented in clusters, using the 40 first UMAP dimensions, Louvain algorithm and a resolution of 0.8. After clustering, only protein coding and IG genes were kept. Mitochondrial genes were removed as well. Clusters were assigned to celltypes using a list of markers (from: ...) and SingleR package with BlueprintEncodeData as reference. 

CLZ Integration: (GSE146771_build.R, Li_gse81861_build.R, CLZ_used_Datasets_Integration_harmony.Rmd)
Zhang data, available in GSE146771 as TPM, were back transformed to counts using relative2abs function of monocle package. 
For Li dataset was retrieved from GSE81861 (link). Counts were imported and processed using preprocess_cds and reduce_dimension from monocle. Batch effect between tissue.type.ch1 was removed using Batchelor (to cite) _CHECK THAT RELEVANT as we use counts_. Counts were imported in Seurat. Empty and duplicated genes were removed.
Both datasets were imported in Seurat with the following parameters: min.cells = 3, min.features = 200. Cell types were assigned using clustify from clustifyr package, based on COLE1 cell types as reference.
Prior to integration, the 3 datasets were reduced to their common genes and merged, then normalised and scaled, regressing out nCount_RNA as covariate. PCA was run on the common variable features. Data were subsequently integrated using harmony, using dataset and patient as variability sources. Harmonised data were further reduced using UMAP on the harmonized pca projections, using the first 40 dimensions. Cells labelled as tumor fibroblasts (as defined in the COLE dataset and extended in the integrated dataset by clustifyr) were further extracted for further analysis.

### Lee

Lee:(Lee_preprocess.Rmd)
Raw UMI counts for the Lee dataset were downloaded from GEO (GSE132465 and GSE132465). Data with min.cells = 3 and min.features = 200 were imported in Seurat. Cells with nFeature_RNA > 200, Feature_RNA < 10000 and percent.mt < 15 were kept and further normalised and scaled, as described for the COLE dataset ( Data were then lognormalized and scaled, regressing out covariates "nCount_RNA", and "percent.mt". PCA reduction was performed using the 2000 most variable features as defined by default parameters of FindVariableFeatures Seurat function). Variation between datasets was corrected using harmony, and UMAP reduction was computed on those corrected components, using the 30 first dimensions. 

### Qian

Qian: (Lambrecht_preprocess_legacy.Rmd)
Counts for the Qian dataset (CRC samples) were downloaded from the authors website (https://lambrechtslab.sites.vib.be/) and processed with their parameters whenever available. Data with min.cells = 10 and min.features = 200 were imported in Seurat. Cells with nFeature_RNA <= 6000 and percent.mt <= 15 were kept and further normalised and scaled, regressing out "nCount_RNA", "percent.mt","S.Score", and "G2M.Score". Variable genes were determined on genes with ExpMean between 0.0125 and 3, and dispersion >3. Dimension were reduced using PCA and UMAP (using the 40 first PC)

### Integration of the 5 datasets

Integration of 5 CRC datasets: (Integrate_5crc.Rmd)
Guo dataset was retrieved from [GSE188711]((https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE188711)), Pelka dataset was retrieved from [GSE178341](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE178341), Qian dataset was retrieved from the authors website (https://lambrechtslab.sites.vib.be/). The 5 datasets contained in Joanito dataset were retrieved from Synapse (syn26844071). Each dataset was preprocessed separately before integration. Briefly, counts were imported for genes present in minimun 3 cells and cells with at least 200 present features (genes). Data were then filered, keeping cells with nFeature_RNA <= 6000, percent.mt <= 10 and nCount_RNA<25000. Data were then normalised and scaled using SCTransform from Seurat, with parameter vst.flavor = "v2", and regressing out "percent.mt". In the case of Joanito dataset, the 5 included datasets were SCTransformed separately. Before integration, a total of 3000 features were determined on the dataset. Datasets were merged and the PCAwas run, on those features. Batch effect between dataset was removed by integrating using harmony, using PCA reduction. Harmonised data were further reduced using UMAP on the corrected pca projections. Clustering was performed using Louvain algorithm, at those resolutions: 0.2, 0.4, 0.6, 0.8, 1.0, 1.2. Cluster cell types were estimated using a list of markers (from: ...) and SingleR package with BlueprintEncodeData as reference. 



