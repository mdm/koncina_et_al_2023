---
title: "Tables"
format:
  html:
    embed-resources: true
    toc: true
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(collapse = TRUE, comment = "#>")
library(targets)
Sys.setenv(TAR_PROJECT = "tables")
```

```{r, include = FALSE}
tar_unscript()
```

```{targets globals, tar_globals = TRUE}
options(tidyverse.quiet = TRUE)
library(tarchetypes)
tar_option_set(packages = c("tidyverse", "rappdirs",
                            "qs", "fs", "huxtable",
                            "readxl", "janitor",
                            "magick", "ggrepel",
                            "gtable", "ggtext",
                            "ggbeeswarm", "grid",
                            "ggh4x", "ggpp",
                            "ek.plot", "ek.data",
                            "ComplexHeatmap",
                            "ggpubr", "rstatix"))

out_path <- file.path("out", Sys.getenv("TAR_PROJECT"))

conflicted::conflicts_prefer(
  dplyr::filter,
  dplyr::rename,
  dplyr::count
)
```


## Supplemental table 1

Gene signatures supplied as supplemental tables and rendered in $\LaTeX$.

```{targets table-s1-function, tar_globals = TRUE}
write_tex_s1 <- function(gs, filename) {
  #f <- get_out_path("table_s1.tex")
 
  map_chr(gs, 
          \(x)  glue::glue_collapse(x, sep = ", ", last = " and ")
  ) |> 
    enframe("signature", "gene symbols") |> 
    as_hux() |> 
    theme_article() |> 
    set_col_width(c(0.2, 0.8)) |> 
    set_italic(2:4, 2, TRUE) |> 
    set_width(0.75) |> 
    huxtable::set_escape_contents(everywhere, 1, FALSE) |> 
    set_valign(everywhere, everywhere, "bottom") |> 
    #huxtable::to_latex()
    huxtable::quick_latex(file = filename)
  latex <- read_file(filename)
  latex <- gsub(pattern = "\\\\documentclass\\{article\\}", 
       replacement = "\\\\documentclass[border=1mm, preview]{standalone}\n\\\\usepackage[active,tightpage]{preview}\n\\\\usepackage{varwidth}\n\\\\usepackage{textgreek}\n",
       latex)
  #body <- str_replace(body, "usepackage\\{tabularx\\}", "usepackage{tabularx}\nusepackage{textgreek}")
  write_file(latex, filename)
  #write_file(body, f)
   
  filename
}
```

```{targets table-s1}
list(
  tar_file_read(
    table_s1_data,
    "genesets.yaml",
    yaml::read_yaml(!!.x)[c("il1b", "icaf", "il1r1_icaf")] |> 
      set_names(c("IL-1\\textbeta", "iCAF", "IL1R1\\textsuperscript{+} iCAF"))
  ),
  
  
  tar_file(
    table_s1_tex,
    write_tex_s1(table_s1_data, file.path(out_path, "table_s1.tex"))
  ),
  
  tar_file(
    table_s1_pdf,
    tinytex::latexmk(table_s1_tex, engine = "xelatex")
  )
)
```

## Supplementary table 3

CAF cell line patient details as requested by the reviewers.

```{targets table-s3}
list(
  
  tar_file(
    table_s3_tex,
    file.path(out_path, "table_s3.tex")
  ),
  
  tar_file(
    table_s3_pdf,
    tinytex::latexmk(table_s3_tex, engine = "xelatex")
  )
)
```


## Supplementary table 4

```{targets table-s4-function, tar_globals = TRUE}
get_table_s4 <- function(table_s4_template, antibody_xls) {
  
  ab_table <- excel_sheets(antibody_xls) |>
    set_names() |> 
    map(
      \(x) read_excel(x, path = antibody_xls)
    ) |> 
    list_rbind(names_to = "group") |> 
    mutate(
      across(c(antigen, manufacturer), 
             \(x) str_replace_all(x, c("&" = "\\\\\\\\&", 
                                       "α" = "\\\\\\\\textalpha ",
                                       "κ" = "\\\\\\\\textkappa ",
                                       "γ" = "\\\\\\\\textgamma ",
                                       "ε" = "\\\\\\\\textepsilon ",
                                       "β" = "\\\\\\\\textbeta "))
      ),
      dilution = if_else(is.na(dilution), "", dilution)
    ) |> 
    group_by(group) |> 
    nest() |> 
    mutate(data = map_chr(data, 
                          \(x) paste(glue::glue_data(x, "\\\\hspace{{1em}}{antigen} & {manufacturer} & {cat_no} & {dilution} & {figure} \\\\\\\\"), collapse = "\n")))
  
  ab_table <- paste(
    "\\\\multicolumn{4}{l}{\\\\textbf{", ab_table$group, "}}\\\\\\\\ \n",
    ab_table$data
  ) |> 
    paste(collapse = "\n")
  
  read_file(table_s4_template) |>
    str_replace("_PLACEHOLDER_", ab_table)
  
  #gsub("_PLACEHOLDER_", paste(ab_table, collapse = "\n"), read_file(table_s4_template))
  
  
}


get_table_s42 <- function(table_s4_template, antibody_xls) {
  
  ab_table <- excel_sheets(antibody_xls) |>
    set_names() |> 
    map(
      \(x) read_excel(x, path = antibody_xls)
    ) |> 
    list_rbind(names_to = "group") |> 
    mutate(
      across(c(antigen, manufacturer), 
             \(x) str_replace_all(x, c("&" = "\\\\\\\\&", 
                                       "α" = "\\\\\\\\textalpha ",
                                       "κ" = "\\\\\\\\textkappa ",
                                       "γ" = "\\\\\\\\textgamma ",
                                       "ε" = "\\\\\\\\textepsilon ",
                                       "β" = "\\\\\\\\textbeta "))
      ),
      dilution = if_else(is.na(dilution), "", dilution)
    ) |> 
    group_by(group) |> 
    nest() |> 
    mutate(data = map_chr(data, 
                          \(x) paste(glue::glue_data(x, "\\\\hspace{{1em}}{antigen} & {manufacturer} & {cat_no} & {dilution} & {figure} \\\\\\\\"), collapse = "\n")))
  
  ab_table <- paste(
    "\\\\SetCell[c=5]{l}{\\\\textbf{", ab_table$group, "}}\\\\\\\\ \n",
    ab_table$data
  ) |> 
    paste(collapse = "\n")
  
  read_file(table_s4_template) |>
    str_replace("_PLACEHOLDER_", ab_table)
  
  #gsub("_PLACEHOLDER_", paste(ab_table, collapse = "\n"), read_file(table_s4_template))
 
  
}
```

```{targets table-s4}
list(
  tar_file(
    table_s4_xls,
    file.path(
      user_data_dir("data"),
      "caf",
      "2023-02-28-caf-il1r1-natcom",
      "antibody_list.xlsx"
    )
  ),
  
  tar_file(
    table_s4_template_tex,
    file.path(
      user_data_dir("data"),
      "caf",
      "2023-02-28-caf-il1r1-natcom",
      "table_s4_template2.tex"
    )
  ),
  
  tar_target(
    table_s4,
    get_table_s42(table_s4_template_tex, table_s4_xls)
  ),
  
  tar_file(
    table_s4_tex,
    {
      f <- file.path(out_path, "table_s42.tex")
      write_lines(table_s4, f)
      f
    }
  ),
  
  tar_file(
    table_s4_pdf,
    tinytex::latexmk(table_s4_tex, engine = "xelatex")
  )
)
```



## Pipeline

```{r}
tar_visnetwork()
```

::: {.callout-note collapse="true"}
## Reproducibility

### Date and time

```{r}
Sys.time()
```

### Repository

```{r}
git2r::repository()
```

### Session info

```{r}
sessionInfo()
```
:::
